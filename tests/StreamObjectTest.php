<?php

namespace OneOfZero\Streams\Tests;

use OneOfZero\Streams\StreamObject;
use PHPUnit_Framework_TestCase;

class StreamObjectTest extends PHPUnit_Framework_TestCase
{
	public function testWriteToInvalidStream()
	{
		$stream = new StreamObject(null);
		$stream->write('foo');
	}
}
