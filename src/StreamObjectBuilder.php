<?php

/**
 * Copyright (c) 2015 Bernardo van der Wal
 * MIT License
 *
 * Refer to the LICENSE file for the full copyright notice.
 */

namespace OneOfZero\Streams;

use Psr\Http\Message\StreamInterface;
use RuntimeException;

class StreamObjectBuilder
{
	/**
	 * @param resource $stream
	 * @param bool     $autoFlush
	 *
	 * @return StreamObject
	 */
	public function wrap($stream, $autoFlush = false)
	{
		return new StreamObject($stream, $autoFlush);
	}

	/**
	 * @param StreamInterface $stream
	 * @param bool            $autoFlush
	 *
	 * @return StreamObject
	 */
	public function wrapPsrStream(StreamInterface $stream, $autoFlush = false)
	{
		return new StreamObject($stream->detach(), $autoFlush);
	}

	/**
	 * @param bool $autoFlush
	 *
	 * @return StreamObject
	 */
	public function memory($autoFlush = false)
	{
		$stream = fopen('php://memory', 'w+');

		return new StreamObject($stream, $autoFlush);
	}

	/**
	 * @return StreamObject
	 */
	public function input()
	{
		$stream = fopen('php://input', 'r');

		return new StreamObject($stream);
	}

	/**
	 * @param bool $autoFlush
	 *
	 * @return StreamObject
	 */
	public function output($autoFlush = false)
	{
		$stream = fopen('php://output', 'r+');

		return new StreamObject($stream, $autoFlush);
	}

	/**
	 * @param string $file
	 * @param string $mode
	 * @param bool   $autoFlush
	 *
	 * @return StreamObject
	 *
	 * @throws RuntimeException
	 */
	public function open($file, $mode, $autoFlush = false)
	{
		$stream = fopen($file, $mode);

		if ($stream === false)
		{
			throw new RuntimeException("Failed opening file \"{$file}\"");
		}

		$length = file_exists($file) ? filesize($file) : 0;

		return new StreamObject($stream, $autoFlush, $length);
	}

	/**
	 * @param string   $host
	 * @param int      $port
	 * @param int|null $timeout
	 * @param bool     $autoFlush
	 *
	 * @return StreamObject
	 */
	public function openSocket($host, $port = null, $timeout = null, $autoFlush = false)
	{
		$port = ($port !== null) ? $port : -1;
		$timeout = ($timeout !== null) ? $timeout : ini_get('default_socket_timeout');

		$errorNumber = 0;
		$errorMessage = '';

		$stream = fsockopen($host, $port, $errorNumber, $errorMessage, $timeout);

		if ($stream === false)
		{
			throw new RuntimeException("Failed opening socket to \"{$host}\": {$errorNumber} - {$errorMessage}");
		}

		return new StreamObject($stream, $autoFlush, 0);
	}

	/**
	 * @param string $command
	 * @param string $mode
	 * @param bool   $autoFlush
	 *
	 * @return StreamObject
	 *
	 * @throws RuntimeException
	 */
	public function openPipe($command, $mode, $autoFlush = false)
	{
		$stream = popen($command, $mode);

		if ($stream === false)
		{
			throw new RuntimeException("Failed creating pipe for command \"{$command}\"");
		}

		return new StreamObject($stream, $autoFlush);
	}

	/**
	 * @param string $command
	 * @param bool   $autoFlush
	 *
	 * @return StreamObject
	 *
	 * @throws RuntimeException
	 */
	public function openInputPipe($command, $autoFlush = false)
	{
		return $this->openPipe($command, 'r', $autoFlush);
	}

	/**
	 * @param string $command
	 * @param bool   $autoFlush
	 *
	 * @return StreamObject
	 *
	 * @throws RuntimeException
	 */
	public function openOutputPipe($command, $autoFlush = false)
	{
		return $this->openPipe($command, 'w', $autoFlush);
	}
}
