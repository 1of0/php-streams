<?php

/**
 * Copyright (c) 2015 Bernardo van der Wal
 * MIT License
 *
 * Refer to the LICENSE file for the full copyright notice.
 */

namespace OneOfZero\Streams;

use Exception;
use Psr\Http\Message\StreamInterface;
use RuntimeException;

class StreamObject implements SharedStreamInterface
{
	const DEFAULT_MAX_WRITE_FAILURES = 5;

	const DEFAULT_BUFFER_SIZE = 8192;

	/**
	 * @var bool $mbFunctionsAvailable
	 */
	private $mbFunctionsAvailable;

	/**
	 * @var resource $innerStream
	 */
	private $innerStream;

	/**
	 * @var bool $autoFlush
	 */
	private $autoFlush;

	/**
	 * @var int $length
	 */
	private $length;

	/**
	 * @var int $maxWriteFailures
	 */
	private $maxWriteFailures = self::DEFAULT_MAX_WRITE_FAILURES;

	/**
	 * @var int $bufferSize
	 */
	private $bufferSize = self::DEFAULT_BUFFER_SIZE;

	/**
	 * @param resource $stream
	 * @param bool $autoFlush
	 * @param int|null $length
	 */
	public function __construct($stream, $autoFlush = false, $length = null)
	{
		$this->mbFunctionsAvailable = function_exists('mb_strlen');
		$this->innerStream = $stream;
		$this->autoFlush = $autoFlush;
		$this->length = $length;
	}

	/**
	 *
	 */
	public function __destruct()
	{
		if ($this->autoFlush)
		{
			try { $this->flush(); } catch (Exception $e) { }
		}

		try { $this->close(); } catch(Exception $e) { }
	}

	/**
	 * {@inheritdoc}
	 */
	public function read($length, $offset = null, $whence = SEEK_SET)
	{
		$this->ensureValidStream();

		$this->seek($offset, $whence);

		$bytesRead = fread($this->innerStream, $length);
		if ($bytesRead === false)
		{
			throw new RuntimeException("Failed reading data from stream");
		}

		return $bytesRead;
	}

	/**
	 * @return string
	 *
	 * @throws RuntimeException
	 */
	public function readAll()
	{
		$this->ensureValidStream();

		$buffer = '';
		$bufferSize = $this->bufferSize;
		while(!$this->eof())
		{
			$buffer .= $this->read($bufferSize);
		}
		return $buffer;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getContents()
	{
		return $this->readAll();
	}

	/**
	 * {@inheritdoc}
	 */
	public function write($data, $length = null, $offset = null, $whence = SEEK_SET)
	{
		$this->ensureValidStream();

		if (!$this->isSeekable() && $offset !== null)
		{
			throw new RuntimeException('Stream is not seekable');
		}

		$this->seek($offset, $whence);

		$bytesToWrite = is_null($length) ? $this->strlen($data) : $length;
		$bytesWrittenTotal = 0;
		$failCounter = 0;

		while ($bytesWrittenTotal < $bytesToWrite)
		{
			$bytesWritten = fwrite(
				$this->innerStream,
				$this->substr($data, $bytesWrittenTotal),
				$bytesToWrite - $bytesWrittenTotal
			);

			if ($bytesWritten === false)
			{
				throw new RuntimeException('Failed writing data to stream');
			}

			$failCounter = ($bytesWritten === 0) ? $failCounter + 1 : 0;

			if ($failCounter === $this->maxWriteFailures)
			{
				throw new RuntimeException("Writing data failed {$failCounter} consecutive times.");
			}

			$bytesWrittenTotal += $bytesWritten;

			if ($bytesWritten > 0 && $this->autoFlush)
			{
				$this->flush();
			}
		}

		return $bytesWrittenTotal;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getSize()
	{
		return $this->length;
	}

	/**
	 * {@inheritdoc}
	 */
	public function eof()
	{
		$this->ensureValidStream();

		return feof($this->innerStream);
	}

	/**
	 * @throws RuntimeException
	 */
	public function flush()
	{
		$this->ensureValidStream();

		if (!fflush($this->innerStream))
		{
			throw new RuntimeException('Failed flushing stream');
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function close()
	{
		$this->ensureValidStream();

		if (!fclose($this->innerStream))
		{
			throw new RuntimeException('Failed closing stream');
		}

		$this->innerStream = null;
	}

	/**
	 * {@inheritdoc}
	 */
	public function seek($position, $whence = SEEK_SET)
	{
		if ($position === null)
		{
			return;
		}

		$this->ensureValidStream();

		if (fseek($this->innerStream, $position, $whence) < 0)
		{
			throw new RuntimeException("Failed to seek to position {$position}");
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function rewind()
	{
		$this->ensureValidStream();

		if (!$this->isSeekable())
		{
			throw new RuntimeException('Stream is not seekable');
		}

		if (!rewind($this->innerStream))
		{
			throw new RuntimeException('Failed to rewind stream');
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function tell()
	{
		$this->ensureValidStream();

		return ftell($this->innerStream);
	}

	/**
	 * @param int $offset
	 * @param int $whence
	 * @throws RuntimeException
	 */
	public function setOffset($offset, $whence = SEEK_SET)
	{
		$this->ensureValidStream();

		$this->seek($offset, $whence);
	}

	/**
	 * @return int
	 */
	public function getOffset()
	{
		return $this->tell();
	}

	/**
	 * {@inheritdoc}
	 */
	public function isSeekable()
	{
		$this->ensureValidStream();

		return $this->getMetadata('seekable');
	}

	/**
	 * {@inheritdoc}
	 */
	public function isReadable()
	{
		$this->ensureValidStream();

		$mode = $this->getMetadata('mode');
		return strpos($mode, 'r') !== false
			|| strpos($mode, '+') !== false;
	}

	/**
	 * {@inheritdoc}
	 */
	public function isWritable()
	{
		$this->ensureValidStream();

		$mode = $this->getMetadata('mode');
		return strpos($mode, 'w') !== false
			|| strpos($mode, 'a') !== false
			|| strpos($mode, 'x') !== false
			|| strpos($mode, 'c') !== false
			|| strpos($mode, '+') !== false;
	}

	/**
	 * @return bool
	 */
	public function isBlocked()
	{
		$this->ensureValidStream();

		return $this->getMetadata('blocked');
	}

	/**
	 * @return bool
	 */
	public function isTimedOut()
	{
		$this->ensureValidStream();

		return $this->getMetadata('timed_out');
	}

	/**
	 * {@inheritdoc}
	 */
	public function detach()
	{
		$stream = $this->innerStream;
		$this->innerStream = null;
		return $stream;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getResource()
	{
		return $this->innerStream;
	}

	/**
	 * @param StreamInterface|StreamObject $target
	 * @param bool $rewindSource
	 * 
	 * @return int
	 */
	public function copyTo(StreamInterface $target, $rewindSource = false)
	{
		$this->ensureValidStream();

		if ($rewindSource && $this->isSeekable())
		{
			$this->rewind();
		}
		$bytesCopied = 0;

		while (!$this->eof())
		{
			$buffer = $this->read($this->bufferSize);
			$bytesCopied += $this->strlen($buffer);
			$target->write($buffer);

			if ($target instanceof StreamObject && $target->autoFlush)
			{
				$target->flush();
			}
		}

		return $bytesCopied;
	}

	/**
	 * @param StreamInterface|StreamObject $source
	 * @param bool $rewindSource
	 *
	 * @return int
	 */
	public function copyFrom(StreamInterface $source, $rewindSource = false)
	{
		$this->ensureValidStream();

		if ($rewindSource && $source->isSeekable())
		{
			$source->rewind();
		}

		$bytesCopied = 0;

		while (!$source->eof())
		{
			$buffer = $source->read($this->bufferSize);
			$bytesCopied += $this->strlen($buffer);
			$this->write($buffer);

			if ($this->autoFlush)
			{
				$this->flush();
			}
		}

		return $bytesCopied;
	}

	/**
	 * @param int|null $offset
	 *
	 * @return int
	 *
	 * @throws RuntimeException
	 */
	public function output($offset = null)
	{
		$this->ensureValidStream();

		$this->seek($offset);

		$bytesOutput = fpassthru($this->innerStream);
		if ($bytesOutput === false)
		{
			throw new RuntimeException('Failed outputting stream data');
		}

		return $bytesOutput;
	}

	/**
	 * @return int
	 */
	public function getMaxWriteFailures()
	{
		return $this->maxWriteFailures;
	}

	/**
	 * @param int $maxWriteFailures
	 */
	public function setMaxWriteFailures($maxWriteFailures)
	{
		$this->maxWriteFailures = $maxWriteFailures;
	}

	/**
	 * @return int
	 */
	public function getBufferSize()
	{
		return $this->bufferSize;
	}

	/**
	 * @param int $bufferSize
	 */
	public function setBufferSize($bufferSize)
	{
		$this->bufferSize = $bufferSize;
	}

	/**
	 * {@inheritdoc}
	 */
	public function __toString()
	{
		$this->ensureValidStream();

		try
		{
			if ($this->isSeekable())
			{
				$this->rewind();
			}
			return $this->readAll();
		}
		catch(Exception $e)
		{
			return '';
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function getMetadata($key = null)
	{
		$this->ensureValidStream();

		$data = stream_get_meta_data($this->innerStream);
		return $data[$key];
	}

	private function ensureValidStream()
	{
		if ($this->innerStream === null  || !is_resource($this->innerStream)) {
			$caller = debug_backtrace()[1]['function'];
			throw new RuntimeException("Attempted {$caller}() on StreamObject without a valid stream");
		}
	}

	/**
	 * @param $input
	 *
	 * @return int
	 *
	 * @throws RuntimeException
	 */
	private function strlen($input)
	{
		if ($this->mbFunctionsAvailable)
		{
			$length = mb_strlen($input, '8bit');
			if ($length === false)
			{
				throw new RuntimeException('mb_strlen() failed');
			}
			return $length;
		}
		else
		{
			return strlen($input);
		}
	}

	/**
	 * @param string $input
	 * @param int $start
	 * @param int|null $length
	 *
	 * @return string
	 */
	private function substr($input, $start, $length = null)
	{
		if ($start === 0 && $length === null)
		{
			return $input;
		}

		if ($this->mbFunctionsAvailable)
		{
			if (!isset($length))
			{
				$length = ($start >= 0) ? $this->strlen($input) - $start : -$start;
			}
			return mb_substr($input, $start, $length, '8bit');
		}

		if (isset($length))
		{
			return substr($input, $start, $length);
		}
		else
		{
			return substr($input, $start);
		}
	}
}
