<?php

/**
 * Copyright (c) 2015 Bernardo van der Wal
 * MIT License
 *
 * Refer to the LICENSE file for the full copyright notice.
 */

namespace OneOfZero\Streams;

use Iterator;
use RuntimeException;

class StreamObjectIterator implements Iterator
{
	/**
	 * @var StreamObjectBuilder $builder
	 */
	private $builder;

	/**
	 * @var string[] $filePaths
	 */
	private $filePaths;

	/**
	 * @var int $count
	 */
	private $count;

	/**
	 * @var string $mode
	 */
	private $mode;

	/**
	 * @var int $index
	 */
	private $index = 0;

	/**
	 * @param StreamObjectBuilder $builder
	 * @param string[] $filePaths
	 * @param string $mode
	 */
	public function __construct(StreamObjectBuilder $builder, array $filePaths, $mode = 'r')
	{
		$this->builder = $builder;
		$this->filePaths = $filePaths;
		$this->count = count($filePaths);
		$this->mode = $mode;
	}

	/**
	 * {@inheritdoc}
	 * 
	 * @return StreamObject
	 *
	 * @throws RuntimeException
	 */
	public function current()
	{
		if ($this->index >= $this->count)
		{
			return null;
		}

		$path = $this->filePaths[$this->index];

		if (!file_exists($path))
		{
			throw new RuntimeException('File does not exist');
		}

		return $this->builder->open($path, $this->mode);
	}

	/**
	 * {@inheritdoc}
	 */
	public function next()
	{
		$this->index++;
	}

	/**
	 * {@inheritdoc}
	 * @return int
	 */
	public function key()
	{
		return $this->index;
	}

	/**
	 * {@inheritdoc}
	 */
	public function valid()
	{
		return $this->index < $this->count;
	}

	/**
	 * {@inheritdoc}
	 */
	public function rewind()
	{
		$this->index = 0;
	}
}