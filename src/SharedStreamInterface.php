<?php

/**
 * Copyright (c) 2015 Bernardo van der Wal
 * MIT License
 *
 * Refer to the LICENSE file for the full copyright notice.
 */

namespace OneOfZero\Streams;

use Psr\Http\Message\StreamInterface;

/**
 * Interface SharedStreamInterface
 *
 * Extends the PSR-7 StreamInterface by providing an additional method that allows to retrieve the inner stream resource
 * without detaching it.
 *
 * @package OneOfZero\Streams
 */
interface SharedStreamInterface extends StreamInterface
{
	/**
	 * Returns the inner stream resource, without disposing the stream.
	 *
	 * @return Resource
	 */
	public function getResource();
}